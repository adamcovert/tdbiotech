$(document).ready(function () {

  $('.burger').on('click', function () {
    $('.page-header__nav').toggleClass('page-header__nav--is-open');
  });

  var scrollEl = document.querySelector('.rellax');

  if (scrollEl) {
    var rellax = new Rellax('.rellax');
  }

  var swiper = new Swiper('.advantages__slider', {
    spaceBetween: 20,
    pagination: {
      el: '.advantages__slider-progressbar',
      type: 'progressbar'
    },
    breakpoints: {
      320: {
        slidesPerView: 1
      },
      600: {
        slidesPerView: 2
      },
      768: {
        slidesPerView: 3,
      },
      992: {
        slidesPerView: 4,
        allowTouchMove: false
      }
    }
  });

  var swiper = new Swiper('.certificates__slider', {
    pagination: {
      el: '.certificates__slider-progressbar',
      type: 'progressbar'
    },
    breakpoints: {
      320: {
        slidesPerView: 2,
        spaceBetween: 10
      },
      576: {
        slidesPerView: 3,
        spaceBetween: 20
      },
      768: {
        slidesPerView: 4,
        spaceBetween: 20
      }
    }
  });

  var galleryThumbs = new Swiper('.product__gallery-thumbs', {
    slidesPerView: 3,
    watchOverflow: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
    direction: 'vertical',
    slidesPerView: 'auto',
    spaceBetween: 15,
  });

  var galleryMain = new Swiper('.product__gallery-main', {
    mousewheel: true,
    watchOverflow: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
    preventInteractionOnTransition: true,
    pagination: {
      el: '.product__gallery-progressbar',
      type: 'progressbar'
    },
    breakpoints: {
      320: {
        direction: 'horizontal'
      },
      992: {
        direction: 'vertical',
      }
    },
    thumbs: {
      swiper: galleryThumbs
    }
  });

  galleryMain.on('slideChangeTransitionStart', function () {
    galleryThumbs.slideTo(galleryMain.activeIndex);
  });

  galleryThumbs.on('transitionStart', function () {
    galleryMain.slideTo(galleryThumbs.activeIndex);
  });

  var swiper = new Swiper('.special-offers__slider', {
    pagination: {
      el: '.special-offers__slider-progressbar',
      type: 'progressbar'
    },
    breakpoints: {
      320: {
        slidesPerView: 2,
        spaceBetween: 10
      },
      576: {
        slidesPerView: 2,
        spaceBetween: 20
      },
      768: {
        slidesPerView: 3,
        spaceBetween: 20
      },
      992: {
        slidesPerView: 4,
        spaceBetween: 20
      }
    }
  });


  var tabs = document.querySelector('.product__tabs');

  if ( tabs ) {

    var supports = 'querySelector' in document && 'addEventListener' in window;
    if ( !supports ) return;

    document.addEventListener('click', function () {

      if ( !event.target.classList.contains('product__tabs-nav-link') ) return;

      var content = document.querySelector(event.target.hash);
      if ( !content ) return;

      event.preventDefault();

      if ( content.classList.contains('product__tabs-content--active') ) return;

      var tabsContent = document.querySelectorAll('.product__tabs-content');
      for (var i = 0; i < tabsContent.length; i++) {
        tabsContent[i].classList.remove('product__tabs-content--active');
      }
      content.classList.add('product__tabs-content--active');

      var tabsLink = document.querySelectorAll('.product__tabs-nav-link');

      for (var i = 0; i < tabsLink.length; i++) {
        tabsLink[i].classList.remove('product__tabs-nav-link--active');
      }

      event.target.classList.add('product__tabs-nav-link--active');

    }, false);

    document.documentElement.className += ' js-tabs';
  };

});